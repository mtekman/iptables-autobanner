.RECIPEPREFIX = >
SHELL=/bin/bash

PROCESS_DIR=address_processing
EV_ADD=evil_addresses
BIN_NAME=evilAddressParser
IPTABLES_LIST_CMD=sudo iptables -nvL

## User can change these 3 (e.g. JOUR=yesterday)
JOUR_RANGE :=
FILTER_NPORTS?=5
FILTER_NUSERS?=5

list:
> @grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' Makefile \
> | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean-processing:  ## Remove just processing files (keeps journal)
> rm -f run_this_iptables.sh $(PROCESS_DIR)/$(EV_ADD).table*
> rm -f $(PROCESS_DIR)/ban.list $(PROCESS_DIR)/current.bans $(PROCESS_DIR)/to_update.txt
> rm -f $(PROCESS_DIR)/new.names $(PROCESS_DIR)/old.names

clean: clean-processing ## Remove all files
> rm -f $(PROCESS_DIR)/$(EV_ADD).txt

$(BIN_NAME):   #-# Compile the address parser
> g++ -o $@ parser/$@.cpp

last_updated.txt:  #-# get the last date of the EV_ADD.txt file
> @if [[ -e $(PROCESS_DIR)/$(EV_ADD).txt ]]; then \
>   stat $(PROCESS_DIR)/$(EV_ADD).txt -c '%y' | awk '{print $$1}' > $@; \
> else \
>   echo "1980-01-01" > $@; \
> fi

$(PROCESS_DIR)/$(EV_ADD).txt: last_updated.txt  #-# Scrape the journal for bad authentication requests
> $(eval JOUR_RANGE := $(shell cat last_updated.txt))
> @echo "1: Getting journal records since last check ($(JOUR_RANGE))"
> mkdir -p $(PROCESS_DIR)
> journalctl -u sshd --since $(JOUR_RANGE) | grep -P "(Failed|Invalid)" | pv >> $@
> @test -s $@ || rm $@ last_updated.txt   ## if output is empty, clear records


$(PROCESS_DIR)/$(EV_ADD).table: $(PROCESS_DIR)/$(EV_ADD).txt $(BIN_NAME)  #-# Parse the addresses
> @echo "2: Parsing for unique addresses"
> cat $< | sed -r 's|(.+): Invalid user ([^ ]+) from ([0-9.]+) port ([0-9]+)|\1: Failed password for \2 from \3 port \4 ssh2|' > $<".fixed"
> ./$(BIN_NAME) $<".fixed" > $@
> @test -s $@ || rm $@

$(PROCESS_DIR)/$(EV_ADD).table.sorted: $(PROCESS_DIR)/$(EV_ADD).table  \
#-# Sort the addresses by Port requests and User requests
> @echo "3: Sorting table"
> cat $< | sort -n -r -k 2 -n -r -k 3 | grep -E -v "(#|Address|=|\s0$$)" > $@
> @test -s $@ || rm $@

$(PROCESS_DIR)/$(EV_ADD).table.sorted.filter: $(PROCESS_DIR)/$(EV_ADD).table.sorted \
#-# Filter addresses by Port and User requests, keeping those above FILTER_NPORTS and FILTER_NUSERS limits.
> @echo "4: Filter addresses that tried > $(FILTER_NPORTS) ports, and > $(FILTER_NUSERS) users."
> cat $< | awk '{if ($$2 > $(FILTER_NPORTS) && $$3 > $(FILTER_NUSERS)){print $$0}}' > $@
> @test -s $@ || rm $@

$(PROCESS_DIR)/ban.list : $(PROCESS_DIR)/$(EV_ADD).table.sorted.filter #-# Generate a ban list based on filters
> @echo "5: Generating ban list"
> awk '{print $$1}' < $< > $@
> @test -s $@ || rm $@

$(PROCESS_DIR)/current.bans:  #-# Extract the current list of banned IPs
> $(IPTABLES_LIST_CMD) | awk '{print $$8}' | grep -P '[0-9.]+' > $@
> @test -s $@ || rm $@

$(PROCESS_DIR)/to_update.txt: $(PROCESS_DIR)/ban.list $(PROCESS_DIR)/current.bans  \
#-# Compare current vs detected IPs to produce a list of new addresses to block
> @echo "6: Checking already banned ips:"
> @cat $(PROCESS_DIR)/current.bans | \
>   grep -E "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+" | sort > $(PROCESS_DIR)/old.names
> @cat $< | grep -E "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+" | sort > $(PROCESS_DIR)/new.names
> @diff -b $(PROCESS_DIR)/old.names $(PROCESS_DIR)/new.names | grep -E "^>" | sed 's/> //g' > $@
> @if [ $$(wc -l < $@) -lt 10 ]; then \
>     echo "Addresses to Update:"; cat $@;\
> else \
>     echo "Updating: $$(wc -l < $@) addresses"; \
> fi
> @test -s $@ || rm $@ && echo " - Nothing new to update. Quitting" && make clean-processing && exit 1

run_this_iptables.sh: $(PROCESS_DIR)/to_update.txt  #-# Generate the iptables block script
> @echo "7: Banning new ips:"
> @echo "" > $@
> @while read address; do \
>    echo -n "--new: $$address"; \
>    echo "iptables -A INPUT -s $$address -j DROP" >> $@; \
>    echo "x"; \
> done < $<
> @test -s $@ || rm $@

perform-ban: run_this_iptables.sh  ## Scan auth logs, generate ban list based on filters, enact the ban
> sudo bash run_this_iptables.sh
> @echo "~~done~~"
.PHONY=perform-ban

list-iptables:   ## List the current iptables rules
> $(IPTABLES_LIST_CMD)


wipe-iptables:       ## Wipes all ban rules (use with caution)
> sudo iptables -F
> sudo iptables -X
> sudo iptables -t nat -F
> sudo iptables -t nat -X
> sudo iptables -t mangle -F
> sudo iptables -t mangle -X
> sudo iptables -t raw -F
> sudo iptables -t raw -X
> sudo iptables -t security -F
> sudo iptables -t security -X
> sudo iptables -P INPUT ACCEPT
> sudo iptables -P FORWARD ACCEPT
> sudo iptables -P OUTPUT ACCEPT
.PHONY=wipe-iptables
